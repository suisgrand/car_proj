using System.Collections.Generic;
using System.Threading.Tasks;
using car_proj.Core.Model;

namespace car_proj.Core.IRepositories
{
    public interface IBrandRepository
    {
        Task<IEnumerable<Brand>> GetAll();

        Task<Brand> Get(int id);

        Task Add(Brand brand);
        Task<bool> Delete(int i);
    }
}
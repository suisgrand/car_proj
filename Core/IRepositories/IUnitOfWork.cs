using System.Threading.Tasks;

namespace car_proj.Core.IRepositories
{
    public interface IUnitOfWork
    {
        Task CompleteAsync();
    }
}
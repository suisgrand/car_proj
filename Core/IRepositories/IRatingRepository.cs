using System.Collections.Generic;
using System.Threading.Tasks;
using car_proj.Core.Model;

namespace car_proj.Core.IRepositories
{
    public interface IRatingRepository
    {
        Task<IEnumerable<Rating>> GetAll();
    }
}
using System.Collections.Generic;
using System.Threading.Tasks;
using car_proj.Core.Model;

namespace car_proj.Core.IRepositories
{
    public interface IMenuRepository
    {
        Task<IEnumerable<FuelType>> GetAllFuelTypes();
        Task<IEnumerable<BodyStyle>> GetAllBodyStyles();
        Task<IEnumerable<Rating>> GetAllRatings();
    }
}
using System.Collections.Generic;
using System.Threading.Tasks;
using car_proj.Core.Model;

namespace car_proj.Core.IRepositories
{
    public interface ICarRepository
    {
        Task<IEnumerable<Car>> GetAll();

        Task<Car> Get(int id);

        Task Add(Car car);
        Task<bool> Delete(int i);

    }
}
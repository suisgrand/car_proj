using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations.Schema;

namespace car_proj.Core.Model
{
    [Table("Brands")]
    public class Brand
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public ICollection<Car> Cars { get; set; }

        public Brand()
        {
            this.Cars = new Collection<Car>();
        }
    }
}
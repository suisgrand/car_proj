namespace car_proj.Core.Model
{
    public class Car
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Length { get; set; }
        public int Width { get; set; }
        public int CylinderNumber { get; set; }
        public int EngineSize { get; set; }
        public BodyStyle BodyStyle { get; set; }
        public int BodyStyleId { get; set; }
        public Brand Brand { get; set; }
        public int BrandId { get; set; }
        public FuelType FuelType { get; set; }
        public int FuelTypeId { get; set; }
        public Rating Rating { get; set; }
        public int RatingId { get; set; }
    }
}
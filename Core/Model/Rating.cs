using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace car_proj.Core.Model
{
    [Table("Ratings")]
    public class Rating
    {
        public int Id { get; set; }
        [Required]
        public string Description { get; set; }
    }
}
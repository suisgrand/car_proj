using System.ComponentModel.DataAnnotations.Schema;

namespace car_proj.Core.Model
{
    [Table("BodyStyles")]
    public class BodyStyle
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
using System.ComponentModel.DataAnnotations.Schema;

namespace car_proj.Core.Model
{
    [Table("FuelTypes")]
    public class FuelType
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
using System.Collections.Generic;
using System.Threading.Tasks;
using car_proj.Core.IRepositories;
using car_proj.Core.Model;
using car_proj.Persistence;
using Microsoft.AspNetCore.Mvc;

namespace car_proj.Controllers
{
    [Route("api/[controller]")]
    public class BrandsController : Controller
    {
        private readonly CarDbContext context;
        private readonly IBrandRepository repository;
        private readonly IUnitOfWork uow;

        public BrandsController(CarDbContext context, IBrandRepository repository, IUnitOfWork uow)
        {
            this.uow = uow;
            this.repository = repository;
            this.context = context;

        }

        [HttpGet("All")]
        public async Task<IEnumerable<Brand>> GetAllCars()
        {
            return await repository.GetAll();
        }

        [HttpPost("Add")]
        public async Task<IActionResult> CreateNewBrand([FromBody] Brand brand)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var result = false;
            var status = string.Empty;

            try
            {
                await repository.Add(brand);
                await uow.CompleteAsync();
                result = true;
            }
            catch (System.Exception e)
            {
                System.Console.WriteLine("Error On CreateNewCar controller endpoing. " + e.StackTrace);
                status = e.StackTrace;
            }
            return Json(new { Success = result, Status = status });
        }


    }
}
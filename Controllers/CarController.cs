using System.Collections.Generic;
using System.Threading.Tasks;
using car_proj.Core.IRepositories;
using car_proj.Core.Model;
using car_proj.Persistence;
using Microsoft.AspNetCore.Mvc;

namespace car_proj.Controllers
{
    [Route("api/[controller]")]
    public class CarController : Controller
    {

        private readonly ICarRepository repository;
        private readonly IUnitOfWork uow;
        private readonly CarDbContext context;

        public CarController(ICarRepository rep, IUnitOfWork unitOFWork, CarDbContext context)
        {
            this.uow = unitOFWork;
            this.repository = rep;
            this.context = context;
        }

        [HttpGet("All")]
        public async Task<IEnumerable<Car>> GetAllCars()
        {
            return await repository.GetAll();
        }

        [HttpGet("GetCar/{id}")]
        public async Task<Car> GetCar(int id)
        {
            var result = await repository.Get(id);
            return result;
        }


        [HttpPost("Add")]
        public async Task<IActionResult> CreateNewCar([FromBody] Car car)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var result = false;
            var status = string.Empty;

            try
            {
                await repository.Add(car);
                await uow.CompleteAsync();
                result = true;
            }
            catch (System.Exception e)
            {
                System.Console.WriteLine("Error On CreateNewCar controller endpoing. " + e.StackTrace);
                status = e.Message;
            }
            return Json(new { Success = result, Status = status });

        }

        [HttpDelete("deleteCar/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            if (id == 0)
                return BadRequest();

            var result = false;
            var status = string.Empty;

            try
            {
                await repository.Delete(id);
                await uow.CompleteAsync();
                result = true;
            }
            catch (System.Exception e)
            {
                System.Console.WriteLine("Error On CreateNewCar controller endpoing. " + e.StackTrace);
                status = e.Message;
            }

            return Json(new { Success = result, Status = status });

        }

        [HttpPut("Update")]
        public async Task<IActionResult> UpdateCar([FromBody] Car car)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var result = false;
            var status = string.Empty;

            try
            {
                var item = await repository.Get(car.Id);
                if (item != null)
                {
                    item.BodyStyleId = car.BodyStyleId;
                    item.BrandId = car.BrandId;
                    item.CylinderNumber = car.CylinderNumber;
                    item.EngineSize = car.EngineSize;
                    item.FuelTypeId = car.FuelTypeId;
                    item.Name = car.Name;
                    item.RatingId = car.RatingId;
                }

                await uow.CompleteAsync();
                result = true;
            }
            catch (System.Exception e)
            {
                System.Console.WriteLine("Error On CreateNewCar controller endpoing. " + e.StackTrace);
                status = e.StackTrace;
            }
            return Json(new { Success = result, Status = status });

        }
    }
}
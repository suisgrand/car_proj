using System.Collections.Generic;
using System.Threading.Tasks;
using car_proj.Core.IRepositories;
using car_proj.Core.Model;
using car_proj.Persistence;
using Microsoft.AspNetCore.Mvc;

namespace car_proj.Controllers
{
    [Route("api/[controller]")]
    public class MemusController : Controller
    {
        private readonly CarDbContext context;
        private readonly IMenuRepository repository;

        public MemusController(CarDbContext context, IMenuRepository repository)
        {
            this.repository = repository;
            this.context = context;
        }

        [HttpGet("fueltypes")]
        public async Task<IEnumerable<FuelType>> GetAllFuelTypes()
        {
            return await repository.GetAllFuelTypes();
        }

        [HttpGet("bodystyles")]
        public async Task<IEnumerable<BodyStyle>> GetAllBodyStyles()
        {
            return await repository.GetAllBodyStyles();
        }
        [HttpGet("ratings")]
        public async Task<IEnumerable<Rating>> GetAllRatings()
        {
            return await repository.GetAllRatings();
        }
    }
}
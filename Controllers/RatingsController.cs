using System.Collections.Generic;
using System.Threading.Tasks;
using car_proj.Core.IRepositories;
using car_proj.Core.Model;
using car_proj.Persistence;
using Microsoft.AspNetCore.Mvc;

namespace car_proj.Controllers
{
    [Route("api/[controller]")]
    public class RatingsController : Controller
    {

        private readonly CarDbContext car;
        private readonly IRatingRepository repository;

        public RatingsController(CarDbContext car, IRatingRepository repository)
        {
            this.repository = repository;
            this.car = car;
            System.Console.WriteLine("**** stephane ****");
        }

        [HttpGet("[action]")]

        public async Task<IEnumerable<Rating>> GetAllRatings()
        {
            return await repository.GetAll();
        }


    }
}
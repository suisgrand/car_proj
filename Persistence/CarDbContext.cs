using car_proj.Core.Model;
using Microsoft.EntityFrameworkCore;

namespace car_proj.Persistence
{
    public class CarDbContext : DbContext
    {
        public CarDbContext(DbContextOptions<CarDbContext> options)
        : base(options)
        {

        }

        public DbSet<BodyStyle> BodyStyles { get; set; }
        public DbSet<Brand> Brands { get; set; }
        public DbSet<Car> Cars { get; set; }
        public DbSet<FuelType> FuelTypes { get; set; }
        public DbSet<Rating> Ratings { get; set; }

    }
}
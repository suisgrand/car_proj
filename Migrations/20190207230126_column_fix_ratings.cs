﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace car_proj.Migrations
{
    public partial class column_fix_ratings : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "Description",
                table: "Ratings",
                nullable: false,
                oldClrType: typeof(int));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "Description",
                table: "Ratings",
                nullable: false,
                oldClrType: typeof(string));
        }
    }
}

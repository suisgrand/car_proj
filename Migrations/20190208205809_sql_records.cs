﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace car_proj.Migrations
{
    public partial class sql_records : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("insert into BodyStyles values ('sedan')");
            migrationBuilder.Sql("insert into BodyStyles values ('hatchback')");

            migrationBuilder.Sql("insert into FuelTypes values ('gas')");
            migrationBuilder.Sql("insert into FuelTypes values ('diesel')");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {

        }
    }
}

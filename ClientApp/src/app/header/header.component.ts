import { Component, OnInit } from '@angular/core';
import { NotificationService } from '../service/notification.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  type: string = null;
  message: string = null;

  constructor(private notifService: NotificationService) { }

  ngOnInit() {
    this.notifService.emmitter.subscribe(data => {
      this.message = data.message;
      this.type = data.type;
      this.reset();
    });
  }

  reset() {
    setTimeout(() => {
      this.type = null;
      this.message = null;
    }, 5000);
  }
}

import { NotificationService } from './../service/notification.service';
import { BrandService } from './../service/brand.service';
import { Component, OnInit } from '@angular/core';
import { Brand } from '../model/Brand';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-brand-detail',
  templateUrl: './brand-detail.component.html',
  styleUrls: ['./brand-detail.component.css']
})
export class BrandDetailComponent implements OnInit {

  brand: Brand = {
    id: 0,
    description: "",
    cars: []
  };

  paramPage;
  NewOrView: string = "";
  form_disable = "";

  constructor(private brandService: BrandService, private route: ActivatedRoute,
    private router: Router,
    private notif: NotificationService) { }

  ngOnInit() {
    this.route.params.subscribe(p => {
      this.paramPage = p['id'];

      if (Number(this.paramPage)) {
        this.NewOrView = "View Brand"
      }
      else if (!this.paramPage) {
        this.NewOrView = "New Brand";
      }
    });
  }

  onSubmit() {
    this.form_disable = "disabled";
    console.log(this.brand);
    this.brandService.createBrand(this.brand).subscribe(data => {
      console.log(data);

      setTimeout(() => {
        this.router.navigate(['/brands']);
      }, 3000);
      this.notif.display('success', 'New Brand Has Been Created');
    }, err => {
      this.form_disable = "";
      this.notif.display('error', err);
    });
  }

}

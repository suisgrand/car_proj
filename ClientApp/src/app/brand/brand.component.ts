
import { Component, OnInit } from '@angular/core';
import { Brand } from '../model/Brand';
import { BrandService } from '../service/brand.service';

@Component({
  selector: 'app-brand',
  templateUrl: './brand.component.html',
  styleUrls: ['./brand.component.css']
})
export class BrandComponent implements OnInit {

  Brands: Brand[] = [];

  constructor(private brandService: BrandService) { }

  ngOnInit() {
    this.brandService.GetAllBrand().subscribe(data => {
      this.Brands = data;
    });
  }

}

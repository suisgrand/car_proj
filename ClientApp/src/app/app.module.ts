import { NotificationService } from './service/notification.service';
import { HeaderComponent } from './header/header.component';

import { BrandDetailComponent } from './brand-detail/brand-detail.component';
import { BrandService } from './service/brand.service';
import { CarService } from './service/car.service';
import { CarComponent } from './car/car/car.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavMenuComponent } from './nav-menu/nav-menu.component';
import { HomeComponent } from './home/home.component';
import { CounterComponent } from './counter/counter.component';
import { FetchDataComponent } from './fetch-data/fetch-data.component';
import { MenuService } from './service/menu.service';
import { BrandComponent } from './brand/brand.component';
import { CarDetailComponent } from './car-detail/car-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    HomeComponent,
    CounterComponent,
    FetchDataComponent,
    CarComponent,
    BrandComponent,
    CarDetailComponent,
    BrandDetailComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: HomeComponent, pathMatch: 'full' },
      { path: 'cars', component: CarComponent },
      { path: 'car/new', component: CarDetailComponent },
      { path: 'car/:id', component: CarDetailComponent },
      { path: 'brands', component: BrandComponent },
      { path: 'brand/new', component: BrandDetailComponent },
      { path: 'brand/:id', component: BrandDetailComponent },
    ])
  ],
  providers: [CarService, MenuService, BrandService, NotificationService],
  bootstrap: [AppComponent]
})
export class AppModule { }

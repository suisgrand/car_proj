import { CarService } from './../service/car.service';
import { BrandService } from './../service/brand.service';
import { Rating } from './../model/Rating';
import { FuelType } from './../model/FuelType';
import { MenuService } from './../service/menu.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Car } from '../model/Car';
import { forkJoin } from 'rxjs';
import { BodyStyle } from '../model/BodyStyle';
import { Brand } from '../model/Brand';
import { NotificationService } from '../service/notification.service';

@Component({
  selector: 'app-car-detail',
  templateUrl: './car-detail.component.html',
  styleUrls: ['./car-detail.component.css']
})
export class CarDetailComponent implements OnInit {

  isNew = false;
  car: Car = {
    id: 0,
    name: "",
    length: 0,
    width: 0,
    cylinderNumber: 0,
    engineSize: 0,
    bodyStyleId: -1,
    brandId: -1,
    fuelTypeId: -1,
    ratingId: -1
  };

  BodyStyleSelected = 1;
  FuelTypeSelectd = 1;
  BrandSelected = 1;
  RatingSeleted = 1;

  fuelTypes: FuelType[] = [];
  bodyStyles: BodyStyle[] = [];
  ratings: Rating[] = [];
  brands: Brand[] = [];

  paramPage;
  NewOrView: string = "";
  form_disable = "";

  constructor(private menuServie: MenuService,
    private route: ActivatedRoute,
    private router: Router,
    private carService: CarService,
    private notif: NotificationService,
    private brandService: BrandService) { }

  ngOnInit() {
    this.form_disable = "disabled";

    this.route.params.subscribe(p => {
      this.paramPage = p['id'];

      if (Number(this.paramPage)) {
        this.NewOrView = "View Car Detail"
        this.GetMenus(+this.paramPage);
      }
      else if (!this.paramPage) {
        this.NewOrView = "Enter New Car";
        this.GetMenus(0);
        this.isNew = true;
      }

    });
  }

  GetMenus(id: number) {

    let arrayServices = [];
    arrayServices.push(this.menuServie.GetAllBodyStyles());
    arrayServices.push(this.menuServie.GetAllFuelTypes());
    arrayServices.push(this.menuServie.GetAllRatings());
    arrayServices.push(this.brandService.GetAllBrand());

    if (id > 0) {
      arrayServices.push(this.carService.GetCar(id));
    }

    forkJoin(arrayServices).subscribe(data => {
      let dataIndex = 0;

      this.bodyStyles = data[dataIndex++];
      this.fuelTypes = data[dataIndex++];
      this.ratings = data[dataIndex++];
      this.brands = data[dataIndex++];

      if (id > 0) {
        this.car = data[dataIndex] as Car;

        console.log("car", this.car);
        this.BodyStyleSelected = this.car.bodyStyleId;
        this.RatingSeleted = this.car.ratingId;
        this.FuelTypeSelectd = this.car.fuelTypeId;
        this.BrandSelected = this.car.brandId;
      }

      console.log(data);
      this.form_disable = "";

    });

  }

  delete() {
    this.carService.DeleteCar(this.car.id).subscribe(data => {
      console.log(data);

      if (data['success'] == true) {
        this.notif.display('success', 'Car has Been Deleted');
        setTimeout(() => {
          this.router.navigate(['/cars']);
        }, 3000);
      }
      else {
        this.form_disable = "";
        this.notif.display('error', data['status']);
      }

    }, err => {
      this.notif.display('error', err);
    });
  }

  onSubmit() {
    this.form_disable = "disabled";
    this.car.bodyStyleId = +this.BodyStyleSelected;
    this.car.ratingId = +this.RatingSeleted;
    this.car.fuelTypeId = +this.FuelTypeSelectd;
    this.car.brandId = +this.BrandSelected;
    console.log(this.car);

    if (this.isNew == true) {
      this.addNewCar();
    }
    else {
      this.updateCar();
    }
  }

  addNewCar() {
    this.carService.CreateCar(this.car).subscribe(data => {
      console.log(data);

      if (data['success'] == true) {
        this.notif.display('success', 'New Brand Has Been Created');
        setTimeout(() => {
          this.router.navigate(['/cars']);
        }, 3000);
      }
      else {
        this.form_disable = "";
        this.notif.display('error', data['status']);
      }

    }, err => {
      this.notif.display('error', err);
    });
  }

  updateCar() {
    this.carService.UpdateCar(this.car).subscribe(data => {
      console.log(data);

      if (data['success'] == true) {
        this.notif.display('success', 'New Brand Has Been Created');
        setTimeout(() => {
          this.router.navigate(['/cars']);
        }, 3000);
      }
      else {
        this.form_disable = "";
        this.notif.display('error', data['status']);
      }

    }, err => {
      this.notif.display('error', err);
    });
  }

}


import { MenuService } from './../../service/menu.service';
import { CarService } from './../../service/car.service';
import { Component, OnInit } from '@angular/core';
import { Car } from '../../model/Car';

@Component({
  selector: 'app-car',
  templateUrl: './car.component.html',
  styleUrls: ['./car.component.css']
})
export class CarComponent implements OnInit {

  Cars: Car[] = [];

  constructor(private carService: CarService, private menuService: MenuService) { }

  ngOnInit() {

    this.carService.GetAllCars().subscribe(data => {
      this.Cars = data;
      console.log(this.Cars);
    });
  }

}

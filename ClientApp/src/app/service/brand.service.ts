import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Brand } from '../model/Brand';

@Injectable({
  providedIn: 'root'
})
export class BrandService {

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) {

  }

  GetAllBrand(): Observable<Brand[]> {
    return this.http.get<Brand[]>(this.baseUrl + 'api/brands/all');
  }

  createBrand(brand: Brand): Observable<Brand> {
    return this.http.post<Brand>(this.baseUrl + 'api/brands/Add', brand);
  }
}

import { Rating } from './../model/Rating';
import { BodyStyle } from './../model/BodyStyle';
import { FuelType } from './../model/FuelType';
import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) {

  }



  GetAllFuelTypes(): Observable<FuelType[]> {
    return this.http.get<FuelType[]>(this.baseUrl + 'api/memus/fueltypes');
  }

  GetAllBodyStyles(): Observable<BodyStyle[]> {
    return this.http.get<BodyStyle[]>(this.baseUrl + 'api/memus/bodystyles');
  }

  GetAllRatings(): Observable<Rating[]> {
    return this.http.get<Rating[]>(this.baseUrl + 'api/memus/ratings');
  }

}

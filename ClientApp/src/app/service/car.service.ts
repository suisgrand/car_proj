import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Car } from '../model/Car';

@Injectable({
  providedIn: 'root'
})
export class CarService {

  constructor(private http: HttpClient, @Inject('BASE_URL') private baseUrl: string) {

  }

  CreateCar(car: Car): Observable<Car> {
    return this.http.post<Car>(this.baseUrl + 'api/car/Add', car);
  }

  UpdateCar(car: Car): Observable<Car> {
    return this.http.put<Car>(this.baseUrl + 'api/car/Update', car);
  }

  GetCar(id: number): Observable<Car> {
    return this.http.get<Car>(this.baseUrl + 'api/car/GetCar' + '/' + id);
  }

  DeleteCar(id: number): Observable<Car> {
    return this.http.delete<Car>(this.baseUrl + 'api/car/deleteCar' + '/' + id);
  }



  GetAllCars(): Observable<Car[]> {
    return this.http.get<Car[]>(this.baseUrl + 'api/car/All');
  }


}

import { Car } from "./Car";

export interface Brand {
    id: number;
    description: string;
    cars: Car[]
}
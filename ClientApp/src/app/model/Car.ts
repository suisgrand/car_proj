
export interface Car {
    id: number;
    name: string;
    length: number;
    width: number;
    cylinderNumber: number;
    engineSize: number;
    bodyStyleId: number;
    brandId: number;
    fuelTypeId: number;
    ratingId: number;
}
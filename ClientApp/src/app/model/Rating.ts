
export interface Rating {
    id: number;
    description: string;
}
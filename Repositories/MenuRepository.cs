using System.Collections.Generic;
using System.Threading.Tasks;
using car_proj.Core.IRepositories;
using car_proj.Core.Model;
using car_proj.Persistence;
using Microsoft.EntityFrameworkCore;

namespace car_proj.Repositories
{
    public class MenuRepository : IMenuRepository
    {
        private readonly CarDbContext context;

        public MenuRepository(CarDbContext context)
        {
            this.context = context;
        }

        public async Task<IEnumerable<BodyStyle>> GetAllBodyStyles()
        {
            return await context.BodyStyles.ToListAsync();
        }

        public async Task<IEnumerable<FuelType>> GetAllFuelTypes()
        {
            return await context.FuelTypes.ToListAsync();
        }

        public async Task<IEnumerable<Rating>> GetAllRatings()
        {
            return await context.Ratings.ToListAsync();
        }
    }
}
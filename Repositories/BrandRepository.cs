using System.Collections.Generic;
using System.Threading.Tasks;
using car_proj.Core.IRepositories;
using car_proj.Core.Model;
using car_proj.Persistence;
using Microsoft.EntityFrameworkCore;

namespace car_proj.Repositories
{
    public class BrandRepository : IBrandRepository
    {
        private readonly CarDbContext context;
        public BrandRepository(CarDbContext context)
        {
            this.context = context;

        }

        public async Task Add(Brand brand)
        {
            await context.AddAsync(brand);
        }

        public async Task<bool> Delete(int id)
        {
            var item = await context.Brands
                .Include(x => x.Cars)
                .FirstOrDefaultAsync(x => x.Id == id);
            if (item != null && item.Cars.Count == 0)
            {
                context.Brands.Remove(item);
                return true;
            }
            return false;
        }

        public async Task<Brand> Get(int id)
        {
            return await context.Brands.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<IEnumerable<Brand>> GetAll()
        {
            return await context.Brands.ToListAsync();
        }
    }
}
using System.Threading.Tasks;
using car_proj.Core.IRepositories;
using car_proj.Persistence;

namespace car_proj.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly CarDbContext context;
        public UnitOfWork(CarDbContext context)
        {
            this.context = context;
        }

        public async Task CompleteAsync()
        {
            await this.context.SaveChangesAsync();
        }
    }
}
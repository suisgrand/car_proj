using System.Collections.Generic;
using System.Threading.Tasks;
using car_proj.Core.IRepositories;
using car_proj.Core.Model;
using car_proj.Persistence;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace car_proj.Repositories
{
    public class CarRepository : ICarRepository
    {
        private readonly CarDbContext context;

        public CarRepository(CarDbContext context)
        {
            this.context = context;
        }

        public async Task Add(Car car)
        {
            await context.Cars.AddAsync(car);
        }

        public async Task<bool> Delete(int id)
        {
            var item = await context.Cars.FirstOrDefaultAsync(x => x.Id == id);
            if (item != null)
            {
                context.Cars.Remove(item);
                return true;
            }
            return false;
        }

        public async Task<Car> Get(int id)
        {
            return await context.Cars.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<IEnumerable<Car>> GetAll()
        {
            return await context.Cars
                .Include(b => b.BodyStyle)
                .ToListAsync();
        }

    }
}
using System.Threading.Tasks;
using car_proj.Core.IRepositories;
using car_proj.Core.Model;
using car_proj.Persistence;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace car_proj.Repositories
{
    public class RatingRepository : IRatingRepository
    {
        private readonly CarDbContext context;

        public RatingRepository(CarDbContext context)
        {
            this.context = context;

        }

        public async Task<IEnumerable<Rating>> GetAll()
        {
            return await context.Ratings.ToListAsync();
        }
    }
}